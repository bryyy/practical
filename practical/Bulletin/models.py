from django.db import models
from django.db.models.functions import Trunc


class Board(models.Model):
    board_name = models.CharField(max_length=250)
    creator = models.CharField(max_length=250)
    created_on = models.DateTimeField('date created')
    pos = models.PositiveIntegerField()

    class Meta:
        ordering = ['-created_on']

    def __str__(self):
        return self.board_name


class Thread(models.Model):
    title = models.CharField(max_length=250)
    post_by = models.CharField(max_length=250)
    post_on = models.DateTimeField('date posted')
    is_locked = models.BooleanField()
    board = models.ForeignKey(Board, on_delete=models.CASCADE)

    class Meta:
        ordering = ['-post_on']

    def __str__(self):
        return self.title


class Post(models.Model):
    message = models.TextField()
    p_post_on = models.DateTimeField('date posted')
    p_post_by = models.CharField(max_length=250)
    thread = models.ForeignKey(Thread, on_delete=models.CASCADE)

    class Meta:
        ordering = ['-p_post_on']

    def __str__(self):
        return self.message
