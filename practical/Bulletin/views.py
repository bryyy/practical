from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views import generic
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from django.contrib.auth.models import User
from django.core.paginator import Paginator

from . models import Board, Thread, Post
from .forms import RegistrationForm, EditForm

from datetime import datetime


def login_user(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('bulletin:home')
    else:
        form = AuthenticationForm()
    context = {'form': form}
    return render(request, 'account/login.html', context)


def logout_user(request):
    logout(request)
    return HttpResponseRedirect(reverse('bulletin:login'))


def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)

        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('bulletin:home')

    else:
        form = RegistrationForm()

    context = {'form': form}
    return render(request, 'account/register.html', context)


def user_profile_check(request, pk):
    user_result = User.objects.get(id=pk)

    context = {'user': user_result}

    return render(request, 'account/profile.html', context)


def bulletin_view(request):
    board_list = Board.objects.order_by('pos')[:20]
    context = {
        'board_list': board_list,
    }
    return render(request, 'account/log_home.html', context)


def profile_view(request):
    context = {'user': request.user}
    return render(request, 'account/profile.html', context)


def profile_edit(request):
    if request.method == 'POST':
        form = EditForm(request.POST, instance=request.user)

        if form.is_valid():
            form.save()
            return redirect('account/profile.html')
    else:
        form = EditForm(instance=request.user)
        context = {'form': form}
        return render(request, 'account/profile_edit.html', context)


def profile_change_pass(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.POST, user=request.user)

        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect('account/profile.html')
    else:
        form = PasswordChangeForm(user=request.user)
        context = {'form': form}
        return render(request, 'account/profile_edit.html', context)


def post_reply(request,board_id, pk):

    redir = '/' + 'board/' + str(board_id) + '/' + str(pk)
    post_message = request.POST['post_message']
    post_on = datetime.now()
    post_by = request.POST['user_name']
    thread = Thread.objects.get(id=pk)
    thread_id = thread

    post_info = Post(message=post_message,
                     p_post_on=post_on,
                     p_post_by=post_by,
                     thread=thread_id)
    if post_message is not None:
        post_info.save()
        return redirect(redir)
    return render(request, 'account/thread.html')


def post_thread(request, pk):

    redir = '/' + 'board/' + str(pk)
    title = request.POST['title']
    is_locked = request.POST['is_locked']
    post_by = request.POST.get('is_locked', False)
    post_on= datetime.now()
    board = Board.objects.get(id=pk)

    thread_info = Thread(title=title,
                         is_locked=is_locked,
                         post_by=post_by,
                         post_on=post_on,
                         board=board)
    thread_info.save()
    return redirect(redir)


def post_board(request):

    creator = request.POST['creator']
    board_name = request.POST['board_name']
    created_on = datetime.now()
    pos_this = request.POST['pos']

    board_info = Board(board_name=board_name,
                       created_on=created_on,
                       creator=creator,
                       pos=pos_this)
    board_info.save()
    return redirect('bulletin:home')


class BoardView(generic.DetailView):
    model = Board
    template_name = 'account/board.html'


def board_view(request, pk):
    board = Board.objects.get(pk=pk)
    return render(request, 'account/board.html', {'board': board})


class ThreadView(generic.ListView):
    model = Thread
    template_name = 'account/thread.html'


def thread_view(request, board_id, pk):
    thread = Thread.objects.get(pk=pk)
    return render(request, 'account/thread.html', {'thread': thread})
