from django.urls import path
from . import views

app_name = 'bulletin'
urlpatterns = [
    path('', views.bulletin_view, name='home'),
    path('register', views.register, name='register'),
    path('login', views.login_user, name="login"),
    path('logout', views.logout_user, name='logout'),
    path('board', views.bulletin_view, name='bulletin'),
    path('board/<int:pk>', views.board_view, name='board'),
    path('board/<int:board_id>/<int:pk>', views.thread_view, name='thread'),
    path('board/<int:board_id>/<int:pk>/post_reply', views.post_reply, name='post_reply'),
    path('profile', views.profile_view, name='profile'),
    path('profile/edit', views.profile_edit, name="edit"),
    path('profile/change_pass', views.profile_change_pass, name='change'),
    path('user/<int:pk>', views.user_profile_check, name='profile_check'),
    path('post_board', views.post_board, name='post_board'),
    path('board/<int:pk>/post_thread', views.post_thread, name='post_thread')

]
